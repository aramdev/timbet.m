var cuponModule = new Vue({
	el: '#cuponModule',
	data: {
		img: '',
		href: '',
		show: false,
		html: document.getElementsByTagName('html')[0]
	},
	methods: {
		showCupon(img, href){
			this.img = img
			this.href = href
			this.show = true
			this.html.classList.add("js-html");
		},
		hideCupon(){
			this.img = ''
			this.href = ''
			this.show = false
			this.html.classList.remove("js-html");
		}
	}
})


