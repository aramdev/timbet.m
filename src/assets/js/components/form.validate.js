Vue.use(VeeValidate, {
	dictionary: {
		en: {
			messages: {
				required: () => 'Это поле обязательно к заполнению',
				email: () => 'Email адрес не корректен',
			},
			custom: {
				name: {
					min: () => 'Поле должно быть не менее 4 символов'
				},
				msg: {
					min: () => 'Поле должно быть не менее 4 символов',
					max: () => 'Поле должно быть не более 150 символов'
				},
				password: {
					min: () => 'Пароль должен содержать не менее 8 символов'
				},
				currentPassword: {
					min: () => 'Пароль должен содержать не менее 8 символов'
				},
				repassword: {
					min: () => 'Пароль должен содержать не менее 8 символов',
					confirmed: () => 'Подтверждение пароля не соответствует.'
				},
				payment: {
					required: () => 'Это поле обязательно для выбора',
				}
			}
		},
	},
  	events: 'blur|keyup|change'
});

VeeValidate.Validator.extend('passLetter', {
    getMessage: field => `Пароль должен содержать не менее 1 буквы`,
    validate: value => {
        var strongRegex = new RegExp("^(?=.*[A-z])");
        return strongRegex.test(value);
    }
});

VeeValidate.Validator.extend('passNum', {
    getMessage: field => `Пароль должен содержать не менее 1 цыфры`,
    validate: value => {
        var strongRegex = new RegExp("^(?=.*[0-9])");
        return strongRegex.test(value);
    }
});

VeeValidate.Validator.extend('passSpec', {
    getMessage: field => `Пароль не должен содержать спец символы`,
    validate: value => {
        var strongRegex = new RegExp("^(?=.*[!@#\$%\^&\*])");
        return !strongRegex.test(value);
    }
});

var contactValidate = new Vue({
	el: '#formsValidate',
	methods: {
		onSubmit(e){
			this.$validator.validateAll().then(res=>{
                if(res) {
                    this.$refs.form.submit()
                }
            })
		},
	}
})