if(document.getElementById('feedback')){
	var feedbackModule = new Vue({
		el: '#feedback',
		data: {
			img: '',
			date: '',
			modalShow: false,
			html: document.getElementsByTagName('html')[0],
			curretImg: 0,
			images: []
		},
		methods: {
			open (i) {
				this.curretImg = i
				this.img = this.images[i].url;
				this.date = this.images[i].date;
				this.modalShow = true
				this.html.classList.remove("js-html");
			},
			hidefeed(){
				this.img = ''
				this.date = ''
				this.curretImg = ''
				this.modalShow = false
				this.html.classList.remove("js-html");
			},
			prev(){
				if(this.curretImg <= 0){
					this.curretImg = this.images.length - 1
				}else{
					this.curretImg = this.curretImg - 1
				}
				this.img = this.images[this.curretImg].url;
				this.date = this.images[this.curretImg].date;
			},
			next(){
				if(this.curretImg >= this.images.length - 1){
					this.curretImg = 0
				}else{
					this.curretImg = this.curretImg + 1
				}
				this.img = this.images[this.curretImg].url;
				this.date = this.images[this.curretImg].date;
			},
		},
		created () {
			//axios.get('../assets/json/feedback.json')
			axios.get('/mob/reviews')
				.then((response) => {
					this.images = response.data;
				})
				.catch( (error) => {
					console.log(error);
				})
				
		}
	})
}
