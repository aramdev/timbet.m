'use strict'

const gulp = require('gulp');
const $    = require('gulp-load-plugins')();

module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
			.pipe($.include())
	        //.pipe($.cached(options.taskName))
	        //.pipe($.jsMinify())
	        .pipe($.rename({
	        	suffix: ".min"
	        }))
	        .pipe(gulp.dest(options.dest))
	};
};