'use strict'

const gulp = require('gulp');
const $    = require('gulp-load-plugins')();

module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
	        .pipe($.include())
	        .pipe($.cached(options.taskName))
	        .pipe($.rubyHaml({
	            encodings: "UTF-8",
	        }).on('error', function(e) { console.log(e.message); }))
	        .pipe($.htmlBeautify({
	            indent_char: '  ', 
	            indent_size: 1
	        }))
	        .pipe(gulp.dest(options.dest));
	};
};