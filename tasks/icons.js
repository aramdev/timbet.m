'use strict'

const gulp = require('gulp');
const $    = require('gulp-load-plugins')();

module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
	        .pipe($.iconfontCss({
	            fontPath: '../fonts/',
	            fontName: 'fonts.icons',
	            path: options.path,
	            targetPath: '../../sass/base/_fonts.icons.scss',
	            cssClass: "icn",
	        }))
	        .pipe($.iconfont({
	            fontName: 'fonts.icons',
	            formats: ['eot', 'svg', 'ttf', 'woff', "woff2"],
	            normalize:true,
	            prependUnicode: true,
	            fontHeight: 1000,
	        }))
	        .pipe(gulp.dest(function(file){
				return file.extname != '.scss' ? `${options.dest}` : './src/assets/sass/base'
			}))
	        
	        
	};
};


    