'use strict'

const gulp = require('gulp');
const $    = require('gulp-load-plugins')();


module.exports = function(options) {
	return function(){
		return gulp.src(options.src)
			.pipe($.sass().on('error', $.sass.logError))
	        .pipe($.cached(options.taskName))
	        .pipe($.autoprefixer({
	        	browsers: ['last 10 versions'],
            	cascade: false
	        }))
	        .pipe($.groupCssMediaQueries())
	        .pipe($.uglifycss())
	        .pipe($.rename({
	        	suffix: ".min"
	        }))
	        .pipe(gulp.dest(options.dest))
	        
	};
};
